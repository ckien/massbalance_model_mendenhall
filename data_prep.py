#!/usr/bin/env python

import glob
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt 
import os.path as osp


#%%

def rastertonumpy(gisraster):
    '''
    converts gisraster to numpyarray and outputs parameters used to convert it 
    back to a gisraster later on
    requires the ArcGIS arcpy module 
    '''
    import arcpy    
    
    left = float(str(arcpy.GetRasterProperties_management(gisraster, 'LEFT')))
    bottom = float(str(arcpy.GetRasterProperties_management(gisraster, 'BOTTOM')))
    cellsize = float(str(arcpy.GetRasterProperties_management(gisraster, 'CELLSIZEX')))

    nparray = arcpy.RasterToNumPyArray(gisraster)

    rows = np.size(nparray, 0)
    cols = np.size(nparray, 1)
    
    return [nparray, rows, cols, left, bottom, cellsize]

def convert_tiff_to_npz(tiff, npz, name, test=1):
    '''
    converts a .tif raster into a .npz file, with 
    name being the name of the variable
    '''
    
    raster_np = rastertonumpy(tiff)[0].astype(float)
    
    # assign nans to values <= 0 
    raster_np[raster_np < 0] = np.nan
    
    vals_to_save = {name: raster_np}

    np.savez(npz, **vals_to_save)
    
    if test == 1:
        
        plt.imshow(np.load(npz)[name], interpolation= 'None', cmap='gray')
        
        plt.show()
        
def convert_grids_to_npz():        
        
    workspace = r'A:\4_Mendenhall\Radiation_Mendenhall'
    
    tifflist = ['Firn_watershed.tif', 'Ice_watershed.tif', 'Watershed.tif', 
                'DEM_hybrid_full.tif', 'DEM_hybrid_full_hs.tif']
    npzlist = ['Firn.npz','Ice.npz', 'Watershed.npz', 'DEM.npz', 'DEM_hs.npz']
    namelist = ['firn', 'ice', 'watershed', 'dem', 'dem_hs'] 
    
    for tiff, npz, name in zip(tifflist, npzlist, namelist):
        
        tiff = osp.join(workspace, tiff)
        npz = osp.join(workspace, npz)
        
        convert_tiff_to_npz(tiff, npz, name, 1)
    
def calculate_radiation_grids():
    '''
    calculates radiation grids using the ArcGIS arcpy module
    '''
    
    import arcpy
    arcpy.CheckOutExtension('spatial')
    from arcpy import env
    
    workspace = r'A:\4_Mendenhall\Radiation_Mendenhall'
    dem = r'A:\4_Mendenhall\Radiation_Mendenhall\DEM_hybrid_full.tif'
    
    env.overwriteOutput = True
    env.workspace = workspace
    
    daylist = range(1, 367)
    hourrange = range(0, 24)
    
    for day in daylist:
        
        for hour in hourrange:
            
            direc = osp.join(env.workspace, 'direct_{}_{}_watts.tif'.format(day, hour))
            
            try:
            
                globrad = arcpy.sa.AreaSolarRadiation(dem, '56.498', '200', 
                                arcpy.sa.TimeWithinDay(day, hour, hour + 1), '1',
                                '0.5', 'NOINTERVAL', '1', 'FROM_DEM', '32', '8', 
                                '8', 'UNIFORM_SKY', '0.3', '0.5', direc)
                
            except:
                # function fails if there is no direct solar radiation
                print str(hour) + ' failed...'
            
        print str(day) + ' done...'
     
def convert_radiation_to_npz():
    '''
    saves the radiation grids into monthly npz packages
    '''
    
    workspace = r'A:\4_Mendenhall\Radiation_Mendenhall\output_new'

    # create hourly date range
    daterange = pd.date_range(pd.datetime(2018,1,1,0,0), pd.datetime(2018,12,31,23,0), 
                              freq='60min')
    
    radiationlist = []
    datelist = []
    month_previous = 1
    
    for date in daterange:
        
        month = date.month
        
        if month <> month_previous:
            
            print str(month_previous) + ' done...'
            
            radiation = np.dstack(radiationlist)
            
            datelist = np.array(datelist)
            
            np.savez(r'A:\4_Mendenhall\Radiation_Mendenhall\radiation_month' + str(month_previous), 
                     radiation=radiation, date=datelist)
            
            radiationlist = []
            datelist = []
        
        # list the file for the corresponding hour
        filelist = glob.glob('{}/direct_{}_{}_watts.tif'.format(workspace, date.dayofyear, date.hour))
        
        # if there is no direct radiation for a certain day/hour combination
        # then there won't be a file available
        if len(filelist) == 1:
                    
            datelist.append([date.dayofyear, date.hour])
                    
            radiation_np = rastertonumpy(filelist[0])[0]
            
            radiationlist.append(radiation_np)
            
        month_previous = month
     
    # to get december saved    
    print str(month_previous) + ' done...'
    
    radiation = np.dstack(radiationlist)
    
    datelist = np.array(datelist)
    
    np.savez(r'A:\4_Mendenhall\Radiation_Mendenhall\radiation_month' + str(month_previous), 
             radiation=radiation, date=datelist)


#%%
        
def read_mass_balance_calibration_data():
       
    df = pd.read_excel(r'B:\4_Mendenhall\Massbalance\Massbalance_2018_calibration.xlsx')
    
    # add new columns    
    df['Date_previous'] = ''
    
    for index, row in df.iterrows():
        
        if row['Melt_cm_ieq'] != 0:
            
            df.set_value(index, 'Date_previous', df.loc[index - 1, 'Date'])

    df = df[df['Melt_cm_ieq'] != 0]  
    
    df = df[['Name', 'Date', 'Date_previous', 'Melt_cm_ieq', 
             'Uncertainty_cm_ieq']] # , 'Melt_cumsum_cm_ieq'
    
    df['Melt_cm_weq'] = df['Melt_cm_ieq'] * 0.9

    df.to_pickle(r'B:\4_Mendenhall\Massbalance\Massbalance_calibration_all.pickle')