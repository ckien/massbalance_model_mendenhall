#!/usr/bin/env python

import spotpy
import massbalance_model
import datetime as dt

#%% calibration via spotpy

class spotpy_setup(object):
    
    ''' The spotpy module allows for advanced calibration procedures (e.g. via Markov Chain Monte Carlo)
    http://fb09-pasig.umwelt.uni-giessen.de/spotpy/Tutorial/8-Algorithm_guide/
    '''
    
    def __init__(self, data_switch):
        '''creates instance of the massbalance model and defines parameter space'''
        
        self.mb_model = massbalance_model.MB_Model(climate_data_switch='basin', 
                                                   melt_model_switch='ETI')
                                                   
        # name, low, high, step, optguess
        self.params = [spotpy.parameter.Uniform('mf_nr', 0 / 24, 6.0 / 24, 0.01 / 24),
                       spotpy.parameter.Uniform('rfi_nr', 0.002 / 24, 0.05 / 24, 0.001 / 24)]
                       
#                       spotpy.parameter.Uniform('rfi_nr',  0.005 / 24, 0.044 / 24, 0.001 / 24, 0.02 / 24),
#                       spotpy.parameter.Uniform('temp_grad', -0.87, -0.57, 0.01, -0.6),
#                       spotpy.parameter.Uniform('precip_factor', 1.0, 1.2, 0.02, 1.0)
                                                                                      
    def parameters(self):
        return spotpy.parameter.generate(self.params)

    def simulation(self, vector):
        '''runs model and returns modeled mass balances'''
        simulations = self.mb_model.run(mf_nr=vector[0], rfi_nr=vector[1])
        
        return simulations

        #rfi_nr=vector[2], temp_grad=vector[3], precip_factor=vector[4]

    def evaluation(self):
        '''returns measured mass balances for the selected mass balance stations'''
        
        df = self.mb_model.df_mb_measured
        df_sel = df[(df['Name'] == 'Main_Glacier') | (df['Name'] == 'Basin_North')] 
        
        # convert from cm to mm        
        return (df_sel['Melt_cm_weq'] * 10).values

    def objectivefunction(self, simulation, evaluation):
        '''calculates rmse between measured and modeled balances'''
        objectivefunction = spotpy.objectivefunctions.rmse(evaluation, simulation)
        return objectivefunction
        
def run_spotpy(key='SCEUA', parallel='seq', rep=1000):
    
    '''
    runs the spotpy calibration
    
    mpi parallelization only works on linux machine, launched from shell
    
    mpirun -c 10 calibration_spotpy.py (10 is the number of cores)
    
    python file needs to be executable (chmod +x calibration_spotpy.py)
    
    dos2unix calibration_spotpy.py in case of problems with file line endings 
        
    parameters
    ----------
    key:
        calibration approach ('MC', 'MCMC', or 'SCEUA')
    parallel:
        'seq' (sequential) or 'mpi' (parallelized)
    rep:
        number of runs
    '''
        
    # data_switch=1 implies we are using climate data from LeConte Glacier
    # as opposed to climate data from Petersburg airport (data_switch=2)
    sp_setup = spotpy_setup(data_switch=1)
    # results = []
    
    # retrieve datestring to create unique name of output database
    datestr = dt.datetime.now().strftime('%Y%m%d_%H%M')

    if key == 'MC':
    
        # run simple Monte Carlo approach
        sampler = spotpy.algorithms.mc(sp_setup, dbname='MendenhallMC_' + datestr, 
                                       dbformat='csv', parallel=parallel)
        
        sampler.sample(rep)
        
        #results.append(sampler.getdata())  
    
    elif key == 'SCEUA':
    
        # run the SCE-UA Shuffled Complex Evolution - University of Arizona algorithm
        # http://fb09-pasig.umwelt.uni-giessen.de/spotpy/Tutorial/8-Algorithm_guide/
        sampler = spotpy.algorithms.sceua(sp_setup, dbname='MendenhallSCEUA_' + datestr, 
                                          dbformat='csv', parallel=parallel, alt_objfun=None)
        
        sampler.sample(rep, ngs=8)
        
        #results.append(sampler.getdata()) 
        
        #spotpy.analyser.plot_parametertrace_algorithms(results, algorithmnames=['SCEUA'], 
        #                                               parameternames=['mf_nr', 'rfs_nr'])
        
    elif key == 'MCMC':
        
        # Markov Chain Monte Carlo
        sampler = spotpy.algorithms.mcmc(sp_setup, dbname='MendenhallMCMC_' + datestr, 
                                         dbformat='csv', parallel=parallel)
        
        sampler.sample(rep, nChains=20)
        
        #results.append(sampler.getdata())
        
        #spotpy.analyser.plot_parametertrace_algorithms(results, algorithmnames=['MCMC'], 
        #                                               parameternames=['mf_nr', 'rfs_nr'])
        
    else:
        raise ValueError('keyword not understood')
        
       
if __name__ == '__main__':

    #run_spotpy()     
    run_spotpy('SCEUA', 'mpi', 4000)