#!/usr/bin/env python

import glob
import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt 

import utilities as utils

colors_ck = utils.colors_ck()

from massbalance_model import MB_Model

try:
    mpl.style.use('classic')
except:
    pass

try:
    # set Arial as standard font
    mpl.rc("font", **{"sans-serif": ["Arial"]})
except:
    pass

#%%

def run_MB_Model():
    '''
    runs the MB_Model class
    '''
    # create an instance of the MB_Model class    
    model = MB_Model(climate_data_switch='airport', melt_model_switch='ETI')
    
    # melt factor and radiation factor of ice for test run
    # divide by 24 to get hourly values    
    mf = 4.46 / 24.0
    rfi = 0.006 / 24.0   
    
    # run the model
    model.run(mf, rfi)
    
    # plot modeled and measured cumulative massbalances
    [fig, ax] = model.plot_massbalances()
    
    # save results (used for paper plot)
    model.mb_main.to_pickle('../../../Paper/maps/data/mb_main.pickle')
    model.df_mb_measured.to_pickle('../../../Paper/maps/data/mb_main_measured.pickle')

#%%
  
def run_ensemble_ETI_model():
    '''
    conducts sensitivity ensemble runs using the best calibration runs and some
    assumptions regarding high and low melt scenarios
    '''
    
    # create empty lists for the ensemble runs
    net_series = []
    net_series_high = []
    net_series_low = []
    
    # read calibration results and chose the ones with the best rmse (smaller 90 mm)
    df_result = pd.read_pickle('./calibration_output/calibration_eti_model/' \
                                'Massbalance_2018_calibration_result_all.pickle')
    
    try:
        df_result = df_result.sort_values(by='rmse')
    except:
        df_result = df_result.sort('rmse')
        
    df_result_sel = df_result[df_result['rmse'] <= 90.0]
    
    # split selected runs into chunks so it can be processed in parallel on
    # several consoles
    total_chunks = 20
    
    if total_chunks > len(df_result_sel):
        raise ValueError('total_chunks must be smaller than the length of the dataframe')
    
    # determine the boundaries of the chunks
    chunk_bounderies = np.linspace(0, len(df_result_sel), total_chunks + 1).astype(int)
    
    # select the chunk over which to iterate 
    chunk_nr = 1
    
    df_result_chunk = df_result_sel.iloc[chunk_bounderies[chunk_nr - 1]: chunk_bounderies[chunk_nr]]
    
    # create an instance of the massbalance model class
    model = MB_Model(climate_data_switch='airport', melt_model_switch='ETI')
    
    counter = 1    
    
    for index, row in df_result_chunk.iterrows():
        
        # empty dictionary for keyword arguments
        kwargs = {}
        
        #---------------------------------        
        
        # normal scenario: same precip as airport, 0.8 deg C cooling per 100 m across 
        # watershed, radiation factor of snow only 80% of ice 
                
        kwargs['mf_nr'] = row['mf']
        kwargs['rfi_nr'] = row['rfi']
                        
        [ablation_main, snow_main, df_gw] = model.run(**kwargs)
        summed = ablation_main + snow_main  
        
        net_series.append(summed)
        
        summed.to_pickle('./ensemble_runs_output/ensemble_runs_ETI/' \
                         'ensemble_run_middle{}_{}.pickle'.format(chunk_nr, counter))
        
        #---------------------------------        
        
        # low melt scenario: 25% more precip, 1.0 deg C cooling per 100 m across 
        # watershed, radiation factor of snow only 60% of ice
        kwargs['sensitivity_test_switch'] = 123
        kwargs['precip_adapt'] = 25
        kwargs['temp_grad_sel'] = -1
        kwargs['rfs_ratio'] = 0.6
        
        [ablation_main, snow_main, df_gw] = model.run(**kwargs)
        summed = ablation_main + snow_main
        net_series_high.append(summed)
        
        summed.to_pickle('./ensemble_runs_output/ensemble_runs_ETI/' \
                         'ensemble_run_high{}_{}.pickle'.format(chunk_nr, counter))
        
        #---------------------------------        
        
        # high melt scenario 20% less precip, only 0.6 deg C cooling per 100 m, 
        # across watershed, radiation factor of snow the same as of ice
        
        kwargs['sensitivity_test_switch'] = 123
        kwargs['precip_adapt'] = -20
        kwargs['temp_grad_sel'] = -0.6
        kwargs['rfs_ratio'] = 1.0        
        
        [ablation_main, snow_main, df_gw] = model.run(**kwargs)
        summed = ablation_main + snow_main
        net_series_low.append(summed)
        
        summed.to_pickle('./ensemble_runs_output/ensemble_runs_ETI/' \
                         'ensemble_run_low{}_{}.pickle'.format(chunk_nr, counter))
        
        print '{} of {} done'.format(counter, len(df_result_chunk))
        
        counter += 1
        
    fig, ax = plt.subplots(1, 1, figsize=(12, 8), facecolor='w')        
        
    for net in net_series:
        ax.plot(net.index, net, color=colors_ck.green)

    for net in net_series_low:
        ax.plot(net.index, net, color=colors_ck.red)
        
    for net in net_series_high:
        ax.plot(net.index, net, color=colors_ck.blue)
        
# run_ensemble_ETI_model()
              
def combine_ensemble_results():
    '''
    merges the pickle files from the ensemble runs and plots the corresponding
    time series. also calculates the mass loss at the basin entrance over the 
    timeperiod between dem1 (from 2006) and dem2 (april 2018)
    '''
    
    fig, ax = plt.subplots(1, 1, figsize=(12, 8), facecolor='w') 
    
    mass_loss_low = []
    mass_loss_high = []
    mass_loss_middle = []
    
    series_low = glob.glob('./ensemble_runs_output/old/ensemble_runs_TI_north/*low*')
    series_high = glob.glob('./ensemble_runs_output/old/ensemble_runs_TI_north/*high*')
    series_middle = glob.glob('./ensemble_runs_output/old/ensemble_runs_TI_north/*middle*')
    
    series = [series_low, series_high, series_middle]
    mass_losses = [mass_loss_low, mass_loss_high, mass_loss_middle] 
    colors = [colors_ck.red, colors_ck.green, colors_ck.blue]
    labels = ['low', 'middle', 'high']
    
    for ss, ml, color, label in zip(series, mass_losses, colors, labels):
        
        for s in ss:
        
            s = pd.read_pickle(s)
        
            ax.plot(s.index, s, color=color, label=label, alpha=0.1)
                    
            # find total mass loss from 2006 images to 2018 lidar
            dem1 = s['2006-07-03 12:00']
            dem2 = s['2018-04-29 12:00']
        
            diff_total = dem2 - dem1 
            
            ml.append(diff_total)
        
    months = mpl.dates.MonthLocator()
    ax.xaxis.set_minor_locator(months)
    years = mpl.dates.YearLocator() 
    ax.xaxis.set_major_locator(years)
    
    ax.set_ylabel('Mass loss (mm w.e.)')
    
    [handles, labels] = utils.clean_up_legend(ax)
    ax.legend(handles, labels, loc='upper right', frameon=1, fontsize=12)
    
    fig.tight_layout()   
    fig.show()
    
    print '2006 to 2018 mass loss'
    print 'middle scenario mean: {} mm weq'.format(np.mean(mass_loss_middle))
    print 'middle scenario max: {} mm weq'.format(np.min(mass_loss_middle))
    print 'middle scenario min: {} mm weq'.format(np.max(mass_loss_middle))
    
    print 'high scenario mean: {} mm weq'.format(np.mean(mass_loss_high))
    print 'high scenario max: {} mm weq'.format(np.min(mass_loss_high))
    print 'high scenario min: {} mm weq'.format(np.max(mass_loss_high))

    print 'low scenario mean: {} mm weq'.format(np.mean(mass_loss_low))
    print 'low scenario max: {} mm weq'.format(np.min(mass_loss_low))
    print 'low scenario min: {} mm weq'.format(np.max(mass_loss_low))
    
#%%    
    
def find_annual_melt():
    '''
    determines annual and seasonal mass balances using the mass balance time series
    '''
            
    series = sorted(glob.glob('./ensemble_runs_output/old/ensemble_runs_TI_north/*middle*'))
    
    # select the run with the lowest rmse
    s = pd.read_pickle(series[0])
    
    fig, ax = plt.subplots(1, 1, figsize=(12, 8), facecolor='w')
    
    ax.plot_date(s.index, s, '-', color='k')
    
    # find total mass loss from 2006 images to 2018 lidar
    dem1 = s['2006-07-03 12:00']
    dem2 = s['2018-04-29 12:00']
    
    diff_total = dem2 - dem1 
    
    print 'total mass loss: {} mm weq from {} to {}'.format(round(diff_total, 1), 
          '2006-07-03 12:00', '2018-04-29 12:00')
    
    for year in range(2006, 2019):
        
        s_sub = s[s.index.year == year]  
        
        s_sub.min()
        s_sub.idxmin()
        
        s_sub.max()
        s_sub.idxmax()
        
        try:
            # get annual balances in the fixed system
            last = s['{}-10-01 00:00'.format(year - 1)]
            this = s['{}-10-01 00:00'.format(year)]
            
            diff = this - last
        except:
            pass
        
        print '-----------------'
        print str(year)
        
        try:
            print 'annual balance fixed system: {} mm weq from {} ' \
            'to {}'.format(round(diff, 1), '{}-10-01 00:00'.format(year - 1), 
            '{}-10-01 00:00'.format(year))
            print 'annual balance floating system: {} mm weq from ' \
            '{} to {}'.format(round(s_sub.min() - min_previous, 1), 
            min_previous_idx, s_sub.idxmin())
            print 'winter balance floating system: {} mm weq from {} ' \
            'to {}'.format(round(s_sub.max() - min_previous, 1), 
            min_previous_idx, s_sub.idxmax())
        except:
            pass        
        
        print 'summer balance floating system: {} mm weq from {} ' \
        'to {}'.format(round(s_sub.min() - s_sub.max(), 1), 
        s_sub.idxmax(), s_sub.idxmin())
#        print '{} - {}'.format(s_sub.idxmax(), s_sub.idxmin())
        
        min_previous = s_sub.min() 
        min_previous_idx = s_sub.idxmin()
        
        ax.plot_date(s_sub.idxmin(), s_sub.min(), color=colors_ck.red)
        ax.plot_date(s_sub.idxmax(), s_sub.max(), color=colors_ck.blue)
    
    months = mpl.dates.MonthLocator()
    ax.xaxis.set_minor_locator(months)
    years = mpl.dates.YearLocator() 
    ax.xaxis.set_major_locator(years)
    
    ax.set_ylabel('Mass loss (mm w.e.)')
    
    plt.tight_layout()
        
#find_annual_melt()
