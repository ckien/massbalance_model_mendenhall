#!/usr/bin/env python

'''
Melt model
'''
import numpy as np
import pandas as pd
import datetime as dt

import matplotlib
import matplotlib.pyplot as plt 

import utilities as utils

colors_ck = utils.colors_ck()

import warnings
warnings.filterwarnings('ignore')

try:
    # set Arial as standard font
    matplotlib.rc("font", **{"sans-serif": ["Arial"]}) # "size": fontsize}
except:
    pass


#%%
 
def utm_to_local(x_utm, y_utm, cellsize=5):

    rows = 312
    cols = 468
    left = 523275.561914
    bottom = 6475715.0172
    cellsize = 25.0
    
    top = bottom + cellsize * rows
    
    x_loc = int((x_utm - left) / cellsize)
    y_loc = int((top - y_utm) / cellsize)
    
    return [x_loc, y_loc] 
    
def plot_basemap(dem, mb_stations):

    fig = plt.figure(figsize=(10.0, 10.0 * (float(dem.shape[0]) /  dem.shape[1])), 
                     facecolor='w', edgecolor='k')
    
    ax1 = fig.add_subplot(1,1,1)
    
    terrain = ax1.imshow(dem, cmap='terrain', interpolation='None')
        
    for station in mb_stations.keys():
        
        [x_loc, y_loc] = utm_to_local(mb_stations[station][0], mb_stations[station][1], 5) 
        
        ax1.plot(x_loc - 0.5, y_loc - 0.5, 'o', color='tomato')
        
    fig.colorbar(terrain, label='Elevation (m a.s.l)' )


def round_to_closest_hour(date_sel):
    '''
    round time to the closest hour
    '''
    if date_sel.minute > 0 and date_sel.minute < 30: 
        date_sel = date_sel.replace(minute=0)
    elif date_sel.minute >= 30:
        date_sel = date_sel.replace(minute=0)
        date_sel = date_sel + dt.timedelta(hours=1)
    else:
        pass
    return date_sel 
       
def temp_transfer_func(airport_temp, key='s'):    
    '''
    models the temperature at the basin weather station as a function
    of the temperature measured at Juneau airport (long-term series)
    see postprocess airport climate data for more info on derivation
    -> compared basin and airport weather station data to determine 
    relationship in form of a linear regression
    function returns a lapse rate, which is easier to understand
    '''
    
    basin_temp = 0.4679 + 0.4586 * airport_temp
    
    # elevation difference between stations is 454 m    
    lapse_rate = (basin_temp - airport_temp) / 4.54
    
    if key == 'w':
        thres = -0.6
    else:
        thres = -0.3
        
    # positive lapse rates imply warming with elevation
    # do not allow for lapse rates smaller than thresholds
    # defined above
    if lapse_rate > thres:
        lapse_rate = thres
        
    return lapse_rate
        
def linear_reservoir(qfirn_t1, qsnow_t1, qice_t1, qrock_t1, r_firn, r_snow, r_ice, r_rock, 
                    ts=1.0, k_firn=300.0, k_snow=30.0, k_ice=5.0, k_rock=5.0):
    '''
    linear reservoir model used in R.Hock's DETIM, described in Hock and Noetzli (1997)

    parameters
    ---------
    ts: 
        timestep [hours]
    k: 
        storage constants for different reservoirs [hours]
    r: 
        water input from melt and rain [m3 per timestep]
    q: 
        water output [m3 per timestep]
    '''
    
    qfirn_t2 = qfirn_t1 * np.exp(-(ts / k_firn)) + r_firn - r_firn * np.exp(-(ts / k_firn))
    qsnow_t2 = qsnow_t1 * np.exp(-(ts / k_snow)) + r_snow - r_snow * np.exp(-(ts / k_snow))
    qice_t2 = qice_t1 * np.exp(-(ts / k_ice)) + r_ice - r_ice * np.exp(-(ts / k_ice))
    qrock_t2 = qrock_t1 * np.exp(-(ts / k_rock)) + r_rock - r_rock * np.exp(-(ts / k_rock))
    
    return [qfirn_t2, qsnow_t2, qice_t2, qrock_t2]
    
class MB_Model(object):    
    '''
    mass balance model for Suicide Basin implemented as a class
    
    parameters
    ------------
    climate_data_switch:
        climate forcing, either from suicide basin ("basin") or Juneau 
        airport ("airport")
    melt_model_switch:
        melt model, either classic temperature index ("TI") or enhanced 
        temperature index ("ETI")
    '''
    
    def __init__(self, climate_data_switch='basin', melt_model_switch='ETI'):
        '''
        loads the required data
        '''
        
        # check switch values             
        if climate_data_switch not in ['basin', 'airport']:
            raise ValueError('climate_data_switch must be "basin" or "airport")')
                        
        if melt_model_switch not in ['TI', 'ETI']:
            raise ValueError('melt_model_switch must be "TI" (TI model) or "ETI" (ETI model)')
                        
        # select date range to model over 
        # use a spring date as a start date since the prescribed snow thickness
        # reflects spring conditions of 2018
        if climate_data_switch == 'basin': 
            # forcing from Suicide Basin, which covers only 2018
            daterange = pd.date_range(pd.datetime(2018, 5, 25, 18, 0), 
                                      pd.datetime(2018, 9, 21, 9, 0), 
                                      freq='60min')
           
        else:
            # forcing from JNU airport, which reaches all the way back to 2006 
            daterange = pd.date_range(pd.datetime(2006, 1, 1, 0, 0), 
                                      pd.datetime(2018, 12, 30, 0, 0), 
                                      freq='60min')
        
        # define extent of modeling domain and grid size
        domain_size = (312, 468)
        self.cell_size = 25
        
        mb_stations = {'Main_Glacier': (528738, 6480050),
                       'Basin_North': (529079, 6480170),
                        'Basin_South': (529077, 6480170)}
           
        dem = np.load('./data/DEM.npz')['dem']
        ice = np.load('./data/Ice.npz')['ice']
        firn = np.load('./data/Firn.npz')['firn']
        watershed = np.load('./data/Watershed.npz')['watershed']
        
        # convert ice raster from 0/1 to nan/1 
        ice[ice == 0] = np.nan
        firn[firn == 0] = np.nan
                     
        # derive rock areas from watershed and ice rasters
        rock = watershed.copy()
        rock[(rock == 0) | (ice == 1)] = np.nan
        
        # convert dem and watershed rasters from 0/1 to nan/1               
        dem[watershed == 0] = np.nan 
        watershed[watershed == 0] = np.nan 
        
        # copy of original dem             
        dem_orig = dem.copy()
        
        # linear regression assumes 50 mm w.e. at 400 m a.s.l. (measured in 2018)
        # and 5000 mm w.e. at 2000 m a.s.l (assumed)
        # this snow grid represents a typical spring situation in the basin
        snow_grid_orig = dem_orig * 3.1 - 1190
        snow_grid_orig[snow_grid_orig < 0] = 0
        snow_grid_orig[snow_grid_orig > 5000] = 5000
        
        # elevation of the weather station in the basin and at the airport
        self.elev_ws_basin = 460
        self.elev_ws_airport = 6 
        
        # temperature and precip at Juneau airport (in UTC time)        
        ws_jnu = pd.read_pickle('./data/airport_2006_2018.pickle') 
        
        # bring UTC to AKDT and extract precipitation 
        ws_jnu.index = ws_jnu.index - dt.timedelta(hours=8)    
        precip = ws_jnu['precip_hourly']
        
        # measurements suggest the basin receives factor 1.6 
        # more precip than the airport (derived by comparing cumsums over the)
        # 2016 season
        precip = precip * 1.6
        
        # temperature data from Suicide Basin (already in AKDT)
        if climate_data_switch == 'basin':
            ws_suib = pd.read_pickle('./data/hobo1_60min.pickle')        
            temp = ws_suib['temperature']
        
        # temperature data from Juneau airport    
        else:
            temp = ws_jnu['temp']
                
        # determine coordinates of mb stations
        [self.x_mg, self.y_mg] = utm_to_local(mb_stations['Main_Glacier'][0], 
                                    mb_stations['Main_Glacier'][1]) 
        [self.x_bn, self.y_bn] = utm_to_local(mb_stations['Basin_North'][0], 
                                    mb_stations['Basin_North'][1]) 
        
        # ETI model
        if melt_model_switch == 'ETI':       
            # grid with all zeros for hours without direct solar radiation           
            self.norad = np.zeros(domain_size) 
            
            self.rf_orig = np.zeros(domain_size) * watershed 
            # filled in the run function np.ones(domain_size) * mf_nr * watershed
            self.mf_orig = None 
            
            # load radiation files into dict
            radiation_files = {}
            
            for month in range(1, 13):
                rf = np.load('./data/radiation/radiation_month{}.npz'.format(month))

                radiation_files[month] = {'dates': rf['date'].tolist(), 
                                          'radiation': rf['radiation']}
                rf.close()
                
            self.radiation_files = radiation_files
               
        # conventional TI model    
        else:
            self.mf_orig = np.zeros(domain_size) * watershed
        
        # random date for plotting grids (used when plotting_switch==1)
        self.date_plot = pd.datetime(2018, 5, 27, 12, 0) 
        
        # load mass balance data (used for calibration and plotting)    
        df_mb_measured = pd.read_pickle('./data/Massbalance_calibration_all.pickle') 
        
        # remove mass balance measurements outside modeling period       
        for index, row in df_mb_measured.iterrows():
            
            date_previous = row['Date_previous']
            date = row['Date']
            
            if (round_to_closest_hour(date) not in daterange or 
                round_to_closest_hour(date_previous) not in daterange):
                
                df_mb_measured.drop(index, inplace=1)
                #print 'delete ' + str(index)
        
        # calculate the mass balance cumsums for each station    
        for name_unique in df_mb_measured.Name.unique():
            df_mb_measured.loc[df_mb_measured['Name'] == name_unique, 'Melt_cumsum_cm_weq'] = \
            df_mb_measured.loc[df_mb_measured['Name'] == name_unique, 'Melt_cm_weq'].cumsum()
            
        # convert from cm weq to mm
        df_mb_measured['Melt_cumsum_mm_weq'] = df_mb_measured['Melt_cumsum_cm_weq'] * 10
            
        # create attributes
        self.mb_stations = mb_stations
        self.daterange = daterange
        self.domain_size = domain_size
        self.dem_orig = dem_orig
        self.snow_grid_orig = snow_grid_orig
        
        self.ice = ice
        self.firn = firn
        self.rock = rock
        self.watershed = watershed
                             
        self.temp = temp
        self.temp_grad = None # filled in by the run function
        self.precip = precip
        
        self.climate_data_switch = climate_data_switch
        self.melt_model_switch = melt_model_switch
        
        self.df_mb_measured = df_mb_measured
        
        # filled in by the run function
        self.ablation_main = None
        self.snow_main = None
        self.df_gw = None
        self.mb_main = None
        self.mb_basin = None
        self.rain_main = None
        self.rain_basin = None
        self.rmse = None
                
    def run(self, mf_nr, rfi_nr, mf_ice=0, calibration_switch=1, plotting_switch=0, 
            sensitivity_test_switch=0, precip_adapt=0, temp_grad_sel=0, 
            rfs_ratio=0, mfs_ratio=0):       
        '''
        run the model
        
        parameters
        ----------
        
        mf_nr:
            melt factor [mm / (degC h)], used for ETI model
        rfi_nr:
            radiation factor of ice [mm m2 / (W degC h)], used for ETI model 
        mf_ice:
            melt factor of ice [mm / (degC h)], used TI model
        calibration_switch: 
            calibration switch, 1 (calibration mode) or 0 (no calibration mode)
        plotting_switch:
            plotting switch, 1 (plot results) or 0 (do not plot results)
        sensitivity_test_switch:
            sensitivity test switch, 1 (conduct test) or 0 (do not)
        precip_adapt:
            precipitation increase in % (100 would double the precip)
            only applied if sensitivity_test_switch == 1
        temp_grad_sel:
            temperature gradient per 100 m elevation, used for temperature distribution
            across the watershed. only applied if sensitivity_test_switch == 1
        rfs_ratio:
            ratio of radiation factor of snow compared to rfi, only applied if 
            sensitivity_test_switch == 1 and model == ETI
        mfs_ratio:
            ratio of radiation factor of snow compared to mf_ice, only applied if 
            sensitivity_test_switch == 1 and model == TI
        
        returns
        ----------        
        if calib_switch is 1: 
            returns numpy array with modeled mass balances at the mb stations.
            modeled mass balances are returned for the time periods for which 
            there are measured mass balances.
            this output is used during calibration, i.e. to calculate differences
            between modeled and measured mass balances.
        if calib_switch is 0:
            returns two series with ablation and accumulation measured at the
            mass balance station on the main glacier, as well as a dataframe containing
            watershed-wide quantities (rain+ablation, runoff, rain, ablation).
        '''
        
        print mf_nr, rfi_nr
                
        if sensitivity_test_switch not in [0, 1, 2, 3, 123]:
            raise ValueError('melt_model_switch must be 0, 1, 2, 3, or 123')
            
        if calibration_switch not in [0, 1]:
            raise ValueError('calibration_switch must be 1 (calibration) or 0 (no calibration)')
            
        if plotting_switch not in [0, 1]:
            raise ValueError('plotting_switch must be 1 (plots) or 0 (no plots)')
                 
        if self.melt_model_switch == 'ETI':
            self.mf_orig = np.ones(self.domain_size) * mf_nr * self.watershed
            
        if sensitivity_test_switch in [1, 123]:
            self.temp_grad = temp_grad_sel 
        else:
            self.temp_grad = -0.6
        
        # lists for result time series at mb points
        snowlist_main = []
        snowlist_basin = []
        rainlist_main = []
        rainlist_basin = []
        ablationlist_main = []
        ablationlist_basin = []
            
        # lists for glacier-wide results 
        rain_abl_gw = []
        rain_gw = []
        abl_gw = []
        runoff_gw = []
        
        month_previous = 0
        year_previous = 0
        
        # start with prescribed snow grid
        snow_grid = self.snow_grid_orig.copy()
        
        for c, date in enumerate(self.daterange, start=1):
            
            if self.melt_model_switch == 'ETI': 
                rf = self.rf_orig.copy()
            
            mf = self.mf_orig.copy()
            
            month = date.month
            year = date.year
    
            # ice thinning rate has been ~2.5 m per year from 2006 to 2018
            # adapt elevation model accordingly        
            if year_previous != year:    
                dem_adapt = self.dem_orig + (2018 - year) * 2.5
            
            # enhanced temperature index model
            if self.melt_model_switch == 'ETI':  
                
                if month_previous != month:
                    # load new dates and radiation files
                    dates = self.radiation_files[month]['dates']
                    radiation = self.radiation_files[month]['radiation']
                    
            # get temperature for selected hour
            temp_ws = self.temp[date]
            
            # airport data
            if self.climate_data_switch == 'airport':
                
                # elevation difference between basin and airport
                elev_diff = self.elev_ws_basin - self.elev_ws_airport
                
                # apply transfer function to simulate temperature at the basin 
                # weather station (only constrained for summer months)
                # assume -0.6 centigrades per 100 m for winter season
                if date.month in [11, 12, 1, 2, 3, 4]: 
                    temp_ws = temp_ws + (elev_diff / 100) * temp_transfer_func(temp_ws, 'w')
#                    
                else:
                    temp_ws = temp_ws + (elev_diff / 100) * temp_transfer_func(temp_ws, 's') 
            
            # calculate temperature distribution across the watershed based on the 
            # temperature measurement at the weather station
            temp_dist = temp_ws + ((dem_adapt - self.elev_ws_basin) / 100.0) * self.temp_grad 
            
            # run the enhanced temperature index model 
            if self.melt_model_switch == 'ETI':                                
                
                # load radiation grid
                # hours without direct solar radiation are not in grid collections
                # causes error, thus try-except
                try:
                    rad = radiation[:, :, dates.index([date.dayofyear, date.hour])]
                except:
                    rad = self.norad
                
                # snow or firn -> radiation factor of snow 
                rf[(snow_grid > 0) | (self.firn == 1)] = rfi_nr * 0.8          
                
                if sensitivity_test_switch in [3, 123]: 
                    rf[(snow_grid > 0) | (self.firn == 1)] = rfi_nr * rfs_ratio
                
                # bare ice -> radiation factor of ice
                rf[(snow_grid <= 0) & (self.firn != 1)] = rfi_nr 
                
                # bare rock -> no ablation
                rf[(snow_grid <= 0) & (self.rock == 1)] = 0 
                mf[(snow_grid <= 0) & (self.rock == 1)] = 0
                
                # calculate actual ablation     
                abl = (mf + rf * rad) * temp_dist
            
            # run the classic temperature index model
            if self.melt_model_switch == 'TI': 
                
                # snow or firn -> melt factor of snow 
                mf[(snow_grid > 0) | (self.firn == 1)] = mf_ice * 0.8
                
                if sensitivity_test_switch in [3, 123]:
                    mf[(snow_grid > 0) | (self.firn == 1)] = mf_ice * mfs_ratio
                
                # bare ice -> melt factor of ice
                mf[(snow_grid <= 0) & (self.firn != 1)] = mf_ice
                # bare rock -> no ablation
                mf[(snow_grid <= 0) & (self.rock == 1)] = 0
                
                # calculate actual ablation     
                abl = mf * temp_dist
            
            # set negative ablation to zero
            abl[abl < 0] = 0
                                
            # get precip from weather station and distribute evenly across watershed 
            # incorporate adaptation of precip amount if needed
            precip_ws = self.precip[date]        
                
            if sensitivity_test_switch in [2, 123]:
                    
                precip_ws = self.precip[date] * ((100 + precip_adapt) / 100.0)
                    
            # calculate precipitation distribution across watershed   
            precip_dist = np.ones(self.domain_size) * precip_ws * watershed
                    
            # determine accumulation factor (af), implementing a gradual (linear) 
            # transition between snow and rain between 0.5 and 2.0 deg C 
            af = (2 - temp_dist) / (2 - 0.5)
            # values above 1 (100%) or below 0 (0%) are set to zero 
            af[af > 1] = 1 
            af[af < 0] = 0
            
            # determine hourly solid and liquid accumulation
            snow = precip_dist * af
            rain = precip_dist - snow
            
            rain_and_abl = rain + abl
            
            # select rain and ablation for different surface types
            # by multiplying rain_and_abl with corresponding boolean masks
            part_rock = rain_and_abl * ((snow_grid <= 0) & (self.rock == 1))
            part_ice = rain_and_abl * ((snow_grid <= 0) & (self.firn != 1) & (self.rock != 1))
            part_firn = rain_and_abl * (self.firn == 1)
            part_snow = rain_and_abl * ((snow_grid > 0) & (self.firn != 1))
                            
            # convert to m3
            r_rock = np.nansum(part_rock) / 1000.0 * self.cell_size ** 2 
            r_ice = np.nansum(part_ice) / 1000.0 * self.cell_size ** 2 
            r_firn = np.nansum(part_firn) / 1000.0 * self.cell_size ** 2 
            r_snow = np.nansum(part_snow) / 1000.0 * self.cell_size ** 2
            
            # # check that the two quantities match
            # print np.nansum(rain_and_abl) / 1000.0 * self.cell_size ** 2
            # print np.nansum([r_rock, r_ice, r_firn, r_snow])
             
            try:
                [qfirn, qsnow, qice, qrock] = linear_reservoir(qfirn, qsnow, qice, qrock, 
                                                                r_firn, r_snow, r_ice, r_rock) 
            except: # first time step has no estimates for q just yet
                [qfirn, qsnow, qice, qrock] = linear_reservoir(100, 100, 100, 100, 
                                                                r_firn, r_snow, r_ice, r_rock)
            
            # sum up the runoff from the four quantities for glacier-wide runoff
            runoff_gw.append(np.nansum([qfirn, qsnow, qice, qrock]))
                    
            # adapt snow grid to differentiate between radiation factor of snow 
            # and ice
            snow_grid = snow_grid + snow - abl
            snow_grid[snow_grid < 0] = 0
        
            # compile watershed-wide quantities
            # convert runoff into m3 per hour, with pixelsize = 25 m
            rain_abl_gw.append(np.nansum(rain_and_abl) / 1000.0 * self.cell_size ** 2)
            rain_gw.append(np.nansum(rain) / 1000.0 * self.cell_size ** 2) 
            abl_gw.append(np.nansum(abl) / 1000.0 * self.cell_size ** 2)
            
            # compile quantities at stations
            ablationlist_main.append(abl[self.y_mg, self.x_mg])
            ablationlist_basin.append(abl[self.y_bn, self.x_bn])
            snowlist_main.append(snow[self.y_mg, self.x_mg])
            snowlist_basin.append(snow[self.y_bn, self.x_bn])
            rainlist_main.append(rain[self.y_mg, self.x_mg])
            rainlist_basin.append(rain[self.y_bn, self.x_bn])
            
            month_previous = month
            year_previous = year
            
            # plot selected grids for visual checks        
            if (plotting_switch == 1) and (date == self.date_plot):
                
                fig_kwargs = {'nrows': 1, 'ncols': 1, 'figsize': (12, 8), 
                              'facecolor': 'w'}
                
                fig, ax = plt.subplots(**fig_kwargs)
                rfl = ax.imshow(((snow_grid <= 0) & (self.rock == 1)) * self.watershed)
                ax.set_title('rock without snow cover')
                
                fig, ax = plt.subplots(**fig_kwargs)
                rfl = ax.imshow(((snow_grid <= 0) & (self.firn != 1) & 
                (self.rock != 1)) * self.watershed)
                ax.set_title('ice without snow cover')
                
                fig, ax = plt.subplots(**fig_kwargs)
                rfl = ax.imshow((self.firn == 1) * self.watershed)
                ax.set_title('firn (may have snow cover)')
                
                fig, ax = plt.subplots(**fig_kwargs)
                rfl = ax.imshow(((snow_grid > 0) & (self.firn != 1)) * self.watershed)
                ax.set_title('snow cover (outside firn area)')
                
                if self.melt_model_switch == 'ETI': 
    
                    fig, ax = plt.subplots(**fig_kwargs)
                    rfl = ax.imshow(rf)
                    fig.colorbar(rfl, label='Radiation factor')
                    ax.set_title(self.date_plot.strftime('%Y-%m-%d %H:%M'))
    
                fig, ax = plt.subplots(**fig_kwargs)
                snowl = ax.imshow(snow_grid)
                fig.colorbar(snowl, label='Snow depth (mm)')
                ax.set_title(self.date_plot.strftime('%Y-%m-%d %H:%M'))
                
                fig, ax = plt.subplots(**fig_kwargs)
                mfl = ax.imshow(mf)
                fig.colorbar(mfl, label='Melt factor')
                ax.set_title(self.date_plot.strftime('%Y-%m-%d %H:%M'))
                
                fig, ax = plt.subplots(**fig_kwargs)
                rainr = ax.imshow(rain)
                fig.colorbar(rainr, label='Hourly rain fall (mm)')
                ax.set_title(self.date_plot.strftime('%Y-%m-%d %H:%M'))
                
                fig, ax = plt.subplots(**fig_kwargs)
                snowr = ax.imshow(snow)
                fig.colorbar(snowr, label='Hourly snow fall (mm)')
                ax.set_title(self.date_plot.strftime('%Y-%m-%d %H:%M'))
                plt.show()
            
        # convert basin-wide values to dataframe   
        df_gw = pd.DataFrame(index=self.daterange, data={'runoff': runoff_gw, 
                                                    'rain_ablation': rain_abl_gw, 
                                                    'rain': rain_gw, 'ablation': abl_gw})
           
        # convert to time series (hourly time intervals)
        ablation_main = pd.Series(np.cumsum(ablationlist_main) * -1, self.daterange)
        ablation_basin = pd.Series(np.cumsum(ablationlist_basin) * -1, self.daterange)
        snow_main = pd.Series(np.cumsum(snowlist_main), self.daterange)
        snow_basin = pd.Series(np.cumsum(snowlist_basin), self.daterange)
        rain_main = pd.Series(np.cumsum(rainlist_main), self.daterange)
        rain_basin = pd.Series(np.cumsum(rainlist_basin), self.daterange)
        
        mb_main = ablation_main + snow_main
        mb_basin = ablation_basin + snow_basin
                
        #--------------------------------------------------------------------------
        
        if calibration_switch == 1:
            
            # select measured mass balances to be used for calibration
            df_sel = self.df_mb_measured[(self.df_mb_measured['Name'] == 
            'Main_Glacier') | (self.df_mb_measured['Name'] == 'Basin_North')]
            
            residual = []
    
            mb_modeled_all = []
            mb_measured_all = []        
            
            for index, row in df_sel.iterrows():
                
                date_previous = row['Date_previous']
                date = row['Date']
                
                melt_measured = row['Melt_cm_weq'] * 10
                
                if row['Name'] == 'Main_Glacier':
                    # sampling the massbalance series
                    melt_modeled = (mb_main[round_to_closest_hour(date)] - 
                                    mb_main[round_to_closest_hour(date_previous)])
                else:
                    melt_modeled = (mb_basin[round_to_closest_hour(date)] - 
                                    mb_basin[round_to_closest_hour(date_previous)])
                
    #            print 'modeled: {}'.format(melt_modeled)
    #            
    #            print 'difference: {}'.format(melt_measured - melt_modeled)
    #            print 'positive number means modeled melt is too negative'
    #            
    #            print '----------------------------------------------------'
                
                mb_modeled_all.append(melt_modeled)
                mb_measured_all.append(melt_measured)   
                
                residual.append(melt_measured - melt_modeled)
                
            rmse = round((np.mean(np.array(residual) ** 2)) ** 0.5, 2)
            
            print rmse
            
            self.rmse = rmse
            
            if plotting_switch == 1:
            
                # create figure with subaxes
                fig, ax = plt.subplots(1, 1, figsize=(7.5, 7.5), facecolor='w')
                
                # plot numbers converted to m w.e.
                ax.plot(np.array(mb_measured_all) / 1000.0, np.array(mb_modeled_all) / 1000.0, 
                        'o', color=colors_ck.red)
                            
                ax.plot([0, -10], [0, -10], '-', color='gray', zorder=-1000)
                
                ax.set_xlim([0, -5])
                ax.set_ylim([0, -5])
                ax.set_xlabel('Massbalance measured (m w.e.)', size=13)
                ax.set_ylabel('Massbalance modeled (m w.e.)', size=13)
                fig.tight_layout()
        
        # fill in attributes        
        self.ablation_main = ablation_main
        self.snow_main = snow_main
        self.df_gw = df_gw
        self.mb_main = mb_main
        self.mb_basin = mb_basin
        self.rain_main = rain_main
        self.rain_basin = rain_basin
                
        if calibration_switch == 1:
            return np.array(mb_modeled_all)
            
        else:
            return [ablation_main, snow_main, df_gw]

    
    def plot_massbalances(self):
        '''
        plots the cumulative mass balance time series and - if available -
        also the measured mass balances as cumulative series
        '''
        
        # create figure 
        fig, ax = plt.subplots(1, 1, figsize=(10, 6.5), facecolor='w')
        
        # plot the modeled cumulative ablation
        ax.plot_date(self.mb_main.index, self.mb_main, '-', 
                     label='Cumulative mass balance at glacier entrance', 
                     color=colors_ck.red)
        ax.plot_date(self.mb_basin.index, self.mb_basin, '-', 
                     label='Cumulative mass balance in basin', color=colors_ck.green)
    
        try:
            # select season-long 2016 measurement by Jamie
            df_mg_16 = self.df_mb_measured[self.df_mb_measured['Name'] == 'Main_Glacier_2016']
            date_sel_mg_16 = df_mg_16.iloc[0, :].loc['Date_previous']
            date_sel_mg_16 = round_to_closest_hour(date_sel_mg_16)
            ax.plot_date(date_sel_mg_16, self.mb_main[date_sel_mg_16], 'o', color='none', 
                         markeredgecolor=colors_ck.red) # plots the first point onto the line
            ax.plot_date(df_mg_16['Date'], df_mg_16['Melt_cumsum_mm_weq'] + self.mb_main[date_sel_mg_16], 
                         'o', color='none', markeredgecolor=colors_ck.red, 
                         label='Measured mass balance at glacier entrance')
        except:
            pass
        
        try:
            # select data from main glacier station
            df_mg = self.df_mb_measured[self.df_mb_measured['Name'] == 'Main_Glacier']
            # select 'Date_previous' column of first row
            date_sel_mg = df_mg.iloc[0, :].loc['Date_previous']
            date_sel_mg = round_to_closest_hour(date_sel_mg)
            ax.plot_date(date_sel_mg, self.mb_main[date_sel_mg], 'o', color='none', 
                         markeredgecolor=colors_ck.red) # plots the first point
            ax.plot_date(df_mg['Date'], df_mg['Melt_cumsum_mm_weq'] + self.mb_main[date_sel_mg], 
                         'o', color='none', markeredgecolor=colors_ck.red, 
                         label='Measured mass balance at glacier entrance')
        except:
            pass
        
        try:
            df_bn = self.df_mb_measured[self.df_mb_measured['Name'] == 'Basin_North']        
            date_sel_bn = df_bn.iloc[0, :].loc['Date_previous']
            date_sel_bn = round_to_closest_hour(date_sel_bn)
            ax.plot_date(date_sel_bn, self.mb_basin[date_sel_bn], 'o', 
                         color='none', markeredgecolor=colors_ck.green)
            ax.plot_date(df_bn['Date'], df_bn['Melt_cumsum_mm_weq'] + self.mb_basin[date_sel_bn], 
                         'o', color='none', markeredgecolor=colors_ck.green, 
                         label='Measured mass balance in basin at wire north')
        except:
            pass
        
        ax.plot_date(self.rain_main.index, self.rain_main, '-', label='Cumulative rainfall at ' \
                    'glacier entrance (mm)', color=colors_ck.orange)
                    
        # plot the dates of the mass balance campaigns
        campaigns = pd.concat([df_bn['Date'], df_bn['Date_previous'], df_mg['Date'], 
                        df_mg['Date_previous']]).unique()

#        ax.plot_date([campaigns, campaigns], [0, 100], '-', 
#                     label='Mass balance campaigns 2018', color='k', lw=2)
        
        [handles, labels] = utils.clean_up_legend(ax)
        # puts the upper left corner of the legend at the 110% (x-extent) and 100% (y-extent)
        ax.legend(handles, labels, loc='lower left', frameon=1, 
                  fontsize=12, numpoints=1) # bbox_to_anchor=(0.1, 0.98)
                  
        ax.set_ylabel('Mass change (mm w.e.)', size=13)

        fig.tight_layout()
        fig.show()
        
        return [fig, ax]
        
    def plot_lake_level(self):
        '''
        plots modeled and measured water level increase in the basin
        match is rather poor likely because there is water flowing from the main 
        glacier into the basin
        '''
        
        # load water level data
        waterlevel_u20_76 = pd.read_pickle('./data/U20_76m_water_60min.pickle')  
        waterlevel_u20_76 = waterlevel_u20_76['2018-05-22 09:00':'2018-07-19 15:00']
        
        doi = pd.to_datetime('2018-05-25 18:00:00')#round_to_closest_hour(daterange[0])
        
        waterlevel_ref = waterlevel_u20_76.loc[doi, 'Level_corrected']

        rain_and_abl = self.df_gw['rain_ablation'].cumsum() / 500000 # divide by 0.5 sqkm basin area
        rain_and_abl = rain_and_abl + (waterlevel_ref - rain_and_abl.loc[doi])
        
        runoff_glacwide = self.df_gw['runoff'].cumsum() / 500000 # divide by 0.5 sqkm basin area
        runoff_glacwide = runoff_glacwide + (waterlevel_ref - runoff_glacwide.loc[doi])
        
        print 'ratio: {}%'.format(self.df_gw['rain_ablation'].cumsum().iloc[-1] * 
               100.0 / self.df_gw['runoff'].cumsum().iloc[-1]) 

        # create figure 
        fig, ax = plt.subplots(1, 1, figsize=(10, 6.5), facecolor='w')
                
        ax.plot_date(rain_and_abl.index, rain_and_abl, '-', 
                      color = colors_ck.red, label='Water level modeled no ' \
                      'runoff delay (m)', lw=1.2)
                      
        ax.plot_date(runoff_glacwide.index, runoff_glacwide, '-', 
                      color = colors_ck.green, label='Water level modeled with ' \
                      'runoff delay (m)', lw=1.2)
                      
        ax.plot_date(waterlevel_u20_76.index, waterlevel_u20_76['Level_corrected'], '-', 
                      color=colors_ck.blue, label='Water level measured (m)', lw=1.2) 
                      
        mindate = pd.to_datetime('2018-05-19 00:00')
        maxdate = pd.to_datetime('2018-07-26 00:00')
        ax.set_xlim(mindate, maxdate)
        ax.set_ylim(400, 450)
        
        ax.set_ylabel('Elevation (m a.s.l.)', size=13)
        
        [handles, labels] = utils.clean_up_legend(ax)
        # puts the upper left corner of the legend at the 110% (x-extent) and 100% (y-extent)
        ax.legend(handles, labels, loc='upper left', frameon=1, fontsize=12, 
                  numpoints=1) # bbox_to_anchor=(0.1, 0.98)
        fig.tight_layout()
        fig.show()
        
def test_MB_Model():
    '''
    tests the MB_Model class
    '''
    # create an instance of the MB_Model class    
    model = MB_Model(climate_data_switch='airport', melt_model_switch='ETI')
    
    # melt factor and radiation factor of ice for test run
    # divide by 24 to get hourly values    
    mf = 4.46 / 24.0
    rfi = 0.006 / 24.0   
    
    # run the model
    model.run(mf, rfi)
    
    # plot modeled and measured cumulative massbalances
    [fig, ax] = model.plot_massbalances()
    model.plot_lake_level()


#
##%%
# 
#def mb_model(mb_stations, dem, firn, ice, watershed, climate_data_switch, 
#             calibration_switch, plotting_switch, mf_ice, melt_model_switch, 
#             mf_nr, rfi_nr, sensitivity_test_switch, precip_adapt, temp_grad_sel, 
#             rfs_ratio, mfs_ratio):
#                 
#    #--------------------------------------------------------------------------
#    # load data (__init__ part of class implementation)
#    
#    # check switch values             
#    if climate_data_switch not in [1, 2]:
#        raise ValueError('climate_data_switch must be 1 (basin) or 2 (airport)')
#        
#    if calibration_switch not in [0, 1]:
#        raise ValueError('calibration_switch must be 1 (calibration) or 0 (no calibration)')
#        
#    if plotting_switch not in [0, 1, 2]:
#        raise ValueError('plotting_switch must be 0 (no plots), 1 (selected plots), '
#        'or 2 (all plots)')  
#        
#    if melt_model_switch not in [0, 1]:
#        raise ValueError('melt_model_switch must be 0 (TI model) or 1 (ETI model)')
#        
#    if sensitivity_test_switch not in [0, 1, 2, 3, 123]:
#        raise ValueError('melt_model_switch must be 0, 1, 2, 3, or 123')
#        
#    # select date range to model over 
#    # use a spring date as a start date since the prescribes snow thickness
#    # reflects spring conditions
#    if climate_data_switch == 1: 
#        # forcing from Suicide Basin, which covers only 2018
#        daterange = pd.date_range(pd.datetime(2018, 5, 25, 18, 0), 
#                                  pd.datetime(2018, 9, 21, 9, 0), freq='60min')
#       
#    else:
#        # forcing from JNU airport, which reaches all the way back to 2006 
#        daterange = pd.date_range(pd.datetime(2015, 6, 1, 0, 0), 
#                                  pd.datetime(2018, 12, 30, 0, 0), freq='60min')
#    
#    # define extent of modeling domain and grid size
#    domain_size = (312, 468)
#    cell_size = 25
#    
#    # convert ice raster from 0/1 to nan/1 
#    ice[ice == 0] = np.nan
#    firn[firn == 0] = np.nan
#                 
#    # derive rock areas from watershed and ice rasters
#    rock = watershed.copy()
#    rock[(rock == 0) | (ice == 1)] = np.nan
#    
#    # convert dem and watershed rasters from 0/1 to nan/1               
#    dem[watershed == 0] = np.nan 
#    watershed[watershed == 0] = np.nan 
#    
#    # copy of original dem             
#    dem_orig = dem.copy()
#    
#    # elevation of the weather station in the basin
#    elev_weatherstation = 460
#    
#    # temperature and precip at Juneau airport (in UTC time)        
#    ws_jnu = pd.read_pickle('./data/airport_2006_2018.pickle') 
#    
#    # bring UTC to local time and extract precipitation 
#    ws_jnu.index = ws_jnu.index - dt.timedelta(hours=8)    
#    precip = ws_jnu['precip_hourly']
#    
#    # temperature data from Suicide Basin (already in local time)
#    if climate_data_switch == 1:       
#                
#        ws_suib = pd.read_pickle('./data/hobo1_60min.pickle')        
#        temp = ws_suib['temperature']
#    
#    # temperature data from airport    
#    else:     
#        elev_weatherstation_airport = 6 
#        temp = ws_jnu['temp']
#        
#    if sensitivity_test_switch in [1, 123]:
#        temp_grad = temp_grad_sel 
#    else:
#        temp_grad = -0.6
#            
#    # determine coordinates of mb stations
#    [x_mg, y_mg] = utm_to_local(mb_stations['Main_Glacier'][0], mb_stations['Main_Glacier'][1]) 
#    [x_bn, y_bn] = utm_to_local(mb_stations['Basin_North'][0], mb_stations['Basin_North'][1]) 
#    
#    # ETI model
#    if melt_model_switch == 1:       
#        # grid with all zeros for hours without direct solar radiation           
#        norad = np.zeros(domain_size) 
#        
#        rf_orig = np.zeros(domain_size) * watershed 
#        mf_orig = np.ones(domain_size) * mf_nr * watershed
#        
#        # load radiation files into dict
#        radiation_files = {}
#        for month in range(1, 13):
#            radiation_files[month] = np.load('./data/radiation/radiation_month{}.npz'.format(month))
#           
#    # conventional TI model    
#    else:
#        mf_orig = np.zeros(domain_size) * watershed
#    
#    # random date for plotting grids (used when plotting_switch==1)
#    date_plot = pd.datetime(2018, 5, 27, 12, 0) 
#    
#    # load mass balance data (used for calibration and plotting)
#    if calibration_switch == 1 or plotting_switch > 0:
#
#        df_mb_measured = pd.read_pickle('./data/Massbalance_calibration_all.pickle') 
#        
#        # remove mass balance measurements outside modeling period       
#        for index, row in df_mb_measured.iterrows():
#            
#            date_previous = row['Date_previous']
#            date = row['Date']
#            
#            if (round_to_closest_hour(date) not in daterange or 
#                round_to_closest_hour(date_previous) not in daterange):
#                
#                df_mb_measured.drop(index, inplace=1)
#                
#                print 'delete ' + str(index)
#        
#        # calculate the cumsums for each station    
#        for name_unique in df_mb_measured.Name.unique():
#            df_mb_measured.loc[df_mb_measured['Name'] == name_unique, 'Melt_cumsum_cm_weq'] = \
#            df_mb_measured.loc[df_mb_measured['Name'] == name_unique, 'Melt_cm_weq'].cumsum()
#            
#        # convert from cm weq to mm
#        df_mb_measured['Melt_cumsum_mm_weq'] = df_mb_measured['Melt_cumsum_cm_weq'] * 10
#    
#    #--------------------------------------------------------------------------
#    # run model (__run__ part of class implementation)
#        
#    # lists for result time series at mb points
#    snowlist_main = []
#    snowlist_basin = []
#    rainlist_main = []
#    rainlist_basin = []
#    ablationlist_main = []
#    ablationlist_basin = []
#        
#    # lists for glacier-wide results 
#    rain_abl_gw = []
#    rain_gw = []
#    abl_gw = []
#    runoff_gw = []
#    
#    month_previous = 0
#    year_previous = 0
#    
#    # start with prescribed snow grid
#    # linear regression assumes 50 mm w.e. at 400 m a.s.l. (measured in 2018)
#    # and 5000 mm w.e. at 2000 m a.s.l (assumed)
#    # this snow grid represents a typical spring situation in the basin
#    snow_grid = dem_orig * 3.1 - 1190
#    snow_grid[snow_grid < 0] = 0
#    snow_grid[snow_grid > 5000] = 5000
#    
#    
#    for c, date in enumerate(daterange, start=1):
#        
#        if melt_model_switch == 1: 
#            rf = rf_orig.copy()
#        
#        mf = mf_orig.copy()
#        
#        month = date.month
#        year = date.year
#
#        # thinning rate has been ~3 m per year from 2006 to 2013
#        # adapt elevation model accordingly        
#        if year_previous != year:    
#            dem_adapt = dem_orig + (2018 - year) * 3.0
#        
#        # enhanced temperature index model
#        if melt_model_switch == 1:  
#            
#            if month_previous != month:
#                
#                # load monthly radiation grids
#                radiation_files_sel = radiation_files[month]
#        
#                dates = radiation_files_sel['date'].tolist()
#                radiation = radiation_files_sel['radiation']
#                
#        # get temperature for selected hour
#        temp_ws = temp[date]
#        
#        # airport data
#        if climate_data_switch == 2:
#            
#            # elevation difference between basin and airport
#            elev_diff = elev_weatherstation - elev_weatherstation_airport
#            
#            if date.month in [10, 11, 12, 1, 2, 3, 4, 5]: 
#                temp_ws = temp_ws + (elev_diff / 100) * -0.6
#                
#            else:
#                # apply transfer function to simulate temperature at the basin 
#                # weather station (only constrained for summer months)
#                temp_ws = temp_ws + (elev_diff / 100) * temp_transfer_func(temp_ws) 
#        
#        # calculate temperature distribution across the watershed based on the 
#        # temperature measurement at the weather station
#        temp_dist = temp_ws + ((dem_adapt - elev_weatherstation) / 100.0) * temp_grad 
#        
#        # run the enhanced temperature index model 
#        if melt_model_switch == 1:                                
#            
#            # load radiation grid
#            # hours without direct solar radiation are not in grid collections
#            # causes error, thus try-except
#            try:
#                rad = radiation[:, :, dates.index([date.dayofyear, date.hour])]
#            except:
#                rad = norad
#            
#            rf[(snow_grid > 0) | (firn == 1)] = rfi_nr * 0.8 # snow or firn -> radiation factor of snow          
#            
#            if sensitivity_test_switch in [3, 123]:
#                rf[(snow_grid > 0) | (firn == 1)] = rfi_nr * rfs_ratio # snow or firn -> radiation factor of snow  
#                
#            rf[(snow_grid <= 0) & (firn != 1)] = rfi_nr # bare ice -> radiation factor of ice
#            
#            rf[(snow_grid <= 0) & (rock == 1)] = 0 # bare rock -> no ablation
#            mf[(snow_grid <= 0) & (rock == 1)] = 0 # bare rock -> no ablation
#            
#            # calculate actual ablation     
#            abl = (mf + rf * rad) * temp_dist
#        
#        # run the classic temperature index model
#        if melt_model_switch == 0: 
#            
#            mf[(snow_grid > 0) | (firn == 1)] = mf_ice * 0.8 # snow or firn -> melt factor of snow 
#            
#            if sensitivity_test_switch in [3, 123]:
#                mf[(snow_grid > 0) | (firn == 1)] = mf_ice * mfs_ratio
#            
#            mf[(snow_grid <= 0) & (firn != 1)] = mf_ice # bare ice -> melt factor of ice
#            mf[(snow_grid <= 0) & (rock == 1)] = 0 # bare rock -> no ablation
#            
#            # calculate actual ablation     
#            abl = mf * temp_dist
#        
#        # set negative ablation to zero
#        abl[abl < 0] = 0
#                            
#        # get precip from weather station and distribute evenly across watershed 
#        # incorporate adaptation of precip amount if needed
#        precip_ws = precip[date]        
#            
#        if sensitivity_test_switch in [2, 123]:
#                
#            precip_ws = precip[date] * ((100 + precip_adapt) / 100.0)
#                
#        # calculate precipitation distribution across watershed   
#        precip_dist = np.ones(domain_size) * precip_ws * watershed
#                
#        # determine accumulation factor (af), implementing a gradual (linear) 
#        # transition between snow and rain between 0.5 and 2.0 deg C 
#        af = (2 - temp_dist) / (2 - 0.5)
#        # values above 1 (100%) or below 0 (0%) are set to zero 
#        af[af > 1] = 1 
#        af[af < 0] = 0
#        
#        # determine hourly solid and liquid accumulation
#        snow = precip_dist * af
#        rain = precip_dist - snow
#        
#        rain_and_abl = rain + abl
#        
#        # select rain and ablation for different surface types
#        # by multiplying rain_and_abl with corresponding boolean masks
#        part_rock = rain_and_abl * ((snow_grid <= 0) & (rock == 1))
#        part_ice = rain_and_abl * ((snow_grid <= 0) & (firn != 1) & (rock != 1))
#        part_firn = rain_and_abl * (firn == 1)
#        part_snow = rain_and_abl * ((snow_grid > 0) & (firn != 1))
#                        
#        # convert to m3
#        r_rock = np.nansum(part_rock) / 1000.0 * cell_size ** 2 
#        r_ice = np.nansum(part_ice) / 1000.0 * cell_size ** 2 
#        r_firn = np.nansum(part_firn) / 1000.0 * cell_size ** 2 
#        r_snow = np.nansum(part_snow) / 1000.0 * cell_size ** 2
#        
#        # # check that the two quantities match
#        # print np.nansum(rain_and_abl) / 1000.0 * cell_size ** 2
#        # print np.nansum([r_rock, r_ice, r_firn, r_snow])
#         
#        try:
#            [qfirn, qsnow, qice, qrock] = linear_reservoir(qfirn, qsnow, qice, qrock, 
#                                                            r_firn, r_snow, r_ice, r_rock) 
#        except: # first time step has no estimates for q just yet
#            [qfirn, qsnow, qice, qrock] = linear_reservoir(100, 100, 100, 100, 
#                                                            r_firn, r_snow, r_ice, r_rock)
#        
#        # sum up the runoff from the four quantities for glacier-wide runoff
#        runoff_gw.append(np.nansum([qfirn, qsnow, qice, qrock]))
#                
#        # adapt snow grid to differentiate between radiation factor of snow 
#        # and ice
#        snow_grid = snow_grid + snow - abl
#        snow_grid[snow_grid < 0] = 0
#    
#        # compile watershed-wide numbers
#        # convert runoff into m3 per hour, with pixelsize = 25 m
#        rain_abl_gw.append(np.nansum(rain_and_abl) / 1000.0 * cell_size ** 2)
#        rain_gw.append(np.nansum(rain) / 1000.0 * cell_size ** 2) 
#        abl_gw.append(np.nansum(abl) / 1000.0 * cell_size ** 2)
#        
#        # compile numbers at stations
#        ablationlist_main.append(abl[y_mg, x_mg])
#        ablationlist_basin.append(abl[y_bn, x_bn])
#        snowlist_main.append(snow[y_mg, x_mg])
#        snowlist_basin.append(snow[y_bn, x_bn])
#        rainlist_main.append(rain[y_mg, x_mg])
#        rainlist_basin.append(rain[y_bn, x_bn])
#        
#        month_previous = month
#        year_previous = year
#        
#        # plot selected grids for visual checks        
#        if (plotting_switch > 1) and (date == date_plot):
#            
#            fig_kwargs = {'nrows': 1, 'ncols': 1, 'figsize': (12, 8), 
#                          'facecolor': 'w'}
#            
#            fig, ax = plt.subplots(**fig_kwargs)
#            rfl = ax.imshow(part_rock)
#            ax.set_title('rock')
#            
#            fig, ax = plt.subplots(**fig_kwargs)
#            rfl = ax.imshow(part_ice)
#            ax.set_title('ice')
#            
#            fig, ax = plt.subplots(**fig_kwargs)
#            rfl = ax.imshow(part_firn)
#            ax.set_title('firn')
#            
#            fig, ax = plt.subplots(**fig_kwargs)
#            rfl = ax.imshow(part_snow)
#            ax.set_title('snow')
#            
#            if melt_model_switch == 1: 
#
#                fig, ax = plt.subplots(**fig_kwargs)
#                rfl = ax.imshow(rf)
#                fig.colorbar(rfl, label='Radiation factor')
#                ax.set_title(date_plot.strftime('%Y-%m-%d %H:%M'))
#
#            fig, ax = plt.subplots(**fig_kwargs)
#            snowl = ax.imshow(snow_grid)
#            fig.colorbar(snowl, label='Snow depth (mm)')
#            ax.set_title(date_plot.strftime('%Y-%m-%d %H:%M'))
#            
#            fig, ax = plt.subplots(**fig_kwargs)
#            mfl = ax.imshow(mf)
#            fig.colorbar(mfl, label='Melt factor')
#            ax.set_title(date_plot.strftime('%Y-%m-%d %H:%M'))
#            
#            fig, ax = plt.subplots(**fig_kwargs)
#            rainr = ax.imshow(rain)
#            fig.colorbar(rainr, label='Hourly rain fall (mm)')
#            ax.set_title(date_plot.strftime('%Y-%m-%d %H:%M'))
#            
#            fig, ax = plt.subplots(**fig_kwargs)
#            snowr = ax.imshow(snow)
#            fig.colorbar(snowr, label='Hourly snow fall (mm)')
#            ax.set_title(date_plot.strftime('%Y-%m-%d %H:%M'))
#            plt.show()
#        
#    # convert basin-wide values to dataframe   
#    df_gw = pd.DataFrame(index=daterange, data={'runoff': runoff_gw, 
#                                                'rain_ablation': rain_abl_gw, 
#                                                'rain': rain_gw, 'ablation': abl_gw})
#       
#    # convert to time series (hourly time intervals)
#    ablation_main = pd.Series(np.cumsum(ablationlist_main) * -1, daterange)
#    ablation_basin = pd.Series(np.cumsum(ablationlist_basin) * -1, daterange)
#    snow_main = pd.Series(np.cumsum(snowlist_main), daterange)
#    snow_basin = pd.Series(np.cumsum(snowlist_basin), daterange)
#    rain_main = pd.Series(np.cumsum(rainlist_main), daterange)
#    rain_basin = pd.Series(np.cumsum(rainlist_basin), daterange)
#    
#    mb_main = ablation_main + snow_main
#    mb_basin = ablation_basin + snow_basin 
#    
#    #--------------------------------------------------------------------------
#    
#    if calibration_switch == 1:
#        
#        df_sel = df_mb_measured[(df_mb_measured['Name'] == 
#        'Main_Glacier') | (df_mb_measured['Name'] == 'Basin_North')]
#        
#        residual = []
#
#        mb_modeled_all = []
#        mb_measured_all = []        
#        
#        for index, row in df_sel.iterrows():
#            
#            date_previous = row['Date_previous']
#            date = row['Date']
#            
#            melt_measured = row['Melt_cm_weq'] * 10
#            
#            if row['Name'] == 'Main_Glacier':
#                # sampling the massbalance series
#                melt_modeled = (mb_main[round_to_closest_hour(date)] - 
#                                mb_main[round_to_closest_hour(date_previous)])
#            else:
#                melt_modeled = (mb_basin[round_to_closest_hour(date)] - 
#                                mb_basin[round_to_closest_hour(date_previous)])
#            
##            print 'modeled: {}'.format(melt_modeled)
##            
##            print 'difference: {}'.format(melt_measured - melt_modeled)
##            print 'positive number means modeled melt is too negative'
##            
##            print '----------------------------------------------------'
#            
#            mb_modeled_all.append(melt_modeled)
#            mb_measured_all.append(melt_measured)   
#            
#            residual.append(melt_measured - melt_modeled)
#            
#        rmse = round((np.mean(np.array(residual) ** 2)) ** 0.5, 2)
#        
#        if plotting_switch > 0:
#        
#            # create figure with subaxes
#            fig, ax = plt.subplots(1, 1, figsize=(7.5, 7.5), facecolor='w')
#            
#            # plot numbers converted to m w.e.
#            ax.plot(np.array(mb_measured_all) / 1000.0, np.array(mb_modeled_all) / 1000.0, 
#                    'o', color=colors_ck.red)
#                        
#            ax.plot([0, -10], [0, -10], '-', color='gray', zorder=-1000)
#            
#            ax.set_xlim([0, -5])
#            ax.set_ylim([0, -5])
#            ax.set_xlabel('Mass loss measured (m w.e.)', size=13)
#            ax.set_ylabel('Mass loss modeled (m w.e.)', size=13)
#            fig.tight_layout()
#
#        
#    #--------------------------------------------------------------------------
#      
#    if plotting_switch > 0:
#        
#        # plots the cumulative mass balance time series and - if available -
#        # also the measured mass balance time series 
#        
#        # create figure 
#        fig, ax = plt.subplots(1, 1, figsize=(10, 6.5), facecolor='w')
#        
#        # plot the modeled cumulative ablation
#        ax.plot_date(mb_main.index, mb_main, '-', 
#                     label='Cumulative mass balance at glacier entrance (mm)', 
#                     color=colors_ck.red)
#        ax.plot_date(mb_basin.index, mb_basin, '-', 
#                     label='Cumulative mass balance in basin (mm)', color=colors_ck.green)
#    
#        try:
#            df_mg_16 = df_mb_measured[df_mb_measured['Name'] == 'Main_Glacier_2016']
#            date_sel_mg_16 = df_mg_16.iloc[0, :].loc['Date_previous']
#            date_sel_mg_16 = round_to_closest_hour(date_sel_mg_16)
#            ax.plot_date(date_sel_mg_16, mb_main[date_sel_mg_16], 'o', color='none', 
#                         markeredgecolor=colors_ck.red) # plots the first point onto the line
#            ax.plot_date(df_mg_16['Date'], df_mg_16['Melt_cumsum_mm_weq'] + mb_main[date_sel_mg_16], 
#                         'o', color='none', markeredgecolor=colors_ck.red, 
#                         label='Measured mass balance at glacier entrance (mm)')
#        except:
#            pass
#        
#        try:
#            # select data from main glacier station
#            df_mg = df_mb_measured[df_mb_measured['Name'] == 'Main_Glacier']
#            # select 'Date_previous' column of first row
#            date_sel_mg = df_mg.iloc[0, :].loc['Date_previous']
#            date_sel_mg = round_to_closest_hour(date_sel_mg)
#            ax.plot_date(date_sel_mg, mb_main[date_sel_mg], 'o', color='none', 
#                         markeredgecolor=colors_ck.red) # plots the first point
#            ax.plot_date(df_mg['Date'], df_mg['Melt_cumsum_mm_weq'] + mb_main[date_sel_mg], 
#                         'o', color='none', markeredgecolor=colors_ck.red, 
#                         label='Measured mass balance at glacier entrance (mm)')
#        except:
#            pass
#        
#        try:
#            df_bn = df_mb_measured[df_mb_measured['Name'] == 'Basin_North']        
#            date_sel_bn = df_bn.iloc[0, :].loc['Date_previous']
#            date_sel_bn = round_to_closest_hour(date_sel_bn)
#            ax.plot_date(date_sel_bn, mb_basin[date_sel_bn], 'o', 
#                         color='none', markeredgecolor=colors_ck.green)
#            ax.plot_date(df_bn['Date'], df_bn['Melt_cumsum_mm_weq'] + mb_basin[date_sel_bn], 
#                         'o', color='none', markeredgecolor=colors_ck.green, 
#                         label='Measured mass balance in basin at wire north - low melt (mm)')
#        except:
#            pass
#        
#        ax.plot_date(rain_main.index, rain_main, '-', label='Cumulative rainfall at ' \
#                    'glacier entrance (mm)', color=colors_ck.orange)
#        
#        [handles, labels] = fck.clean_up_legend(ax)
#        # puts the upper left corner of the legend at the 110% (x-extent) and 100% (y-extent)
#        ax.legend(handles, labels, loc='lower left', frameon=1, 
#                  fontsize=12, numpoints=1) # bbox_to_anchor=(0.1, 0.98)
#                  
#        ax.set_ylabel('Mass change (mm)')
#
#        plt.tight_layout()
#        plt.show()
#        
#        #----------------------------------------------------------------------
#        
#        # plots modeled and measured water level increase
#        
#        # load water level data
#        waterlevel_u20_76 = pd.read_pickle('./data/U20_76m_water_60min.pickle')  
#        waterlevel_u20_76 = waterlevel_u20_76['2018-05-22 09:00':'2018-07-19 15:00']
#        
#        doi = pd.to_datetime('2018-05-25 18:00:00')#round_to_closest_hour(daterange[0])
#        
#        waterlevel_ref = waterlevel_u20_76.loc[doi, 'Level_corrected']
#
#        rain_and_abl = df_gw['rain_ablation'].cumsum() / 500000 # divide by 0.5 sqkm basin area
#        rain_and_abl = rain_and_abl + (waterlevel_ref - rain_and_abl.loc[doi])
#        
#        runoff_glacwide = df_gw['runoff'].cumsum() / 500000 # divide by 0.5 sqkm basin area
#        runoff_glacwide = runoff_glacwide + (waterlevel_ref - runoff_glacwide.loc[doi])
#        
#        print 'ratio: {}%'.format(df_gw['rain_ablation'].cumsum().iloc[-1] * 
#               100.0 / df_gw['runoff'].cumsum().iloc[-1]) 
#
#        # create figure 
#        fig, ax = plt.subplots(1, 1, figsize=(10, 6.5), facecolor='w')
#                
#        ax.plot_date(rain_and_abl.index, rain_and_abl, '-', 
#                      color = colors_ck.red, label='Water level modeled no ' \
#                      'runoff delay (m)', lw=1.2)
#                      
#        ax.plot_date(runoff_glacwide.index, runoff_glacwide, '-', 
#                      color = colors_ck.green, label='Water level modeled with ' \
#                      'runoff delay (m)', lw=1.2)
#                      
#        ax.plot_date(waterlevel_u20_76.index, waterlevel_u20_76['Level_corrected'], '-', 
#                      color=colors_ck.blue, label='Water level measured (m)', lw=1.2) 
#                      
#        mindate = pd.to_datetime('2018-05-19 00:00')
#        maxdate = pd.to_datetime('2018-07-26 00:00')
#        
#        # plot the dates of the mass balance campaigns
#        campaigns = pd.concat([df_bn['Date'], df_bn['Date_previous'], df_mg['Date'], 
#                        df_mg['Date_previous']]).unique()
#
#        ax.plot_date([campaigns, campaigns], [400, 401], '-', 
#                     label='Mass balance campaigns', color='k', lw=2)
#        
#        ax.set_xlim(mindate, maxdate)
#        ax.set_ylim(400, 450)
#        
#        [handles, labels] = fck.clean_up_legend(ax)
#        # puts the upper left corner of the legend at the 110% (x-extent) and 100% (y-extent)
#        ax.legend(handles, labels, loc='upper left', frameon=1, fontsize=12, 
#                  numpoints=1) # bbox_to_anchor=(0.1, 0.98)
#        plt.tight_layout()
#        plt.show()
#        
#    if calibration_switch == 1: 
#        return rmse
#        
#    else:
#        return [ablation_main, snow_main, df_gw]
#
#
#     
#
##%%
#
#mb_stations = {'Main_Glacier': (528738, 6480050),
#               'Basin_North': (529079, 6480170),
#                'Basin_South': (529077, 6480170)}
#   
#dem = np.load('./data/DEM.npz')['dem']
#ice = np.load('./data/Ice.npz')['ice']
#firn = np.load('./data/Firn.npz')['firn']
#watershed = np.load('./data/Watershed.npz')['watershed']
#    
#kwargs1 = {'mb_stations': mb_stations, 
#          'dem': dem,
#          'ice': ice, 
#          'firn': firn,
#          'watershed': watershed} 
#
#def test_mb_model(kwargs1):
#    '''
#    tests the mb_model function
#    '''
#        
#    # melt factor and radiation factor of ice for modeling tests
#    # divide by 24 to get hourly values    
#    mf = 4.46 / 24.0 # 4.46 / 24.0
#    rfi = 0.006 / 24.0 # 0.006 / 24.0
#        
#    mf_ice = 5.3 / 24.0 # 5.3 / 24.0 # 5.65 / 24.0  
#        
#    kwargs2 = {'climate_data_switch': 1, # 1=basin, 2=airport
#              'calibration_switch': 1, # 1=yes, 0=no
#              'plotting_switch': 2, # 1=yes, 0=no, 2=also plots modeled grids for a certain hour and day
#              'mf_ice': mf_ice, # melt factor for ice for TI-model  
#              'melt_model_switch': 1,  # 1=ETI-model, 0=TI-model 
#              'mf_nr': mf, # melt factor for ETI-model 
#              'rfi_nr': rfi, # radiation factor for ice for ETI-model 
#               # 0=no test, 1=only temp, 2=only precip, 3=only snow melt or radiation factor, 123=all
#              'sensitivity_test_switch': 0, 
#              'precip_adapt': 0, # precipitation increase in % decrease (used in case sensitiviy test == 2)
#              'temp_grad_sel': 0, # temperature gradient in centigrades (used in case sensitiviy test == 1)
#              'rfs_ratio': 0, # ratio of radiation factor of snow vs ice (used in case sensitiviy test == 3 and ETI model)
#              'mfs_ratio': 0} # ratio of melt factor of snow vs ice (used in case sensitiviy test == 3 and TI model) 
#                             
#    kwargs = dict(kwargs1.items() + kwargs2.items())
#    
#    mb_model(**kwargs)
#    
##    kwargs['sensitivity_test_switch'] = 0
##    kwargs['temp_grad_sel'] = -0.25
##    kwargs['mfs_ratio'] = 1.0
#    
##    kwargs['sensitivity_test_switch'] = 2
##    kwargs['precip_adapt'] = 100
##    kwargs['mfs_ratio'] = 1.0
#         
#    [ablation_main, snow_main, df_gw] = mb_model(**kwargs)
#    
#    plt.figure()
#    df_gw['rain_ablation'].plot(color=colors_ck.blue)
#    
##    runoff_gw = runoff_gw * 5    
#    
#    df_gw['runoff'].plot(color=colors_ck.red)
#    
##    runoff_gw.plot(), rain_gw.plot(), abl_gw.plot()
##    df_gw = pd.concat([runoff_gw, rain_gw, abl_gw], axis=1)
##    df_gw.columns = ['total_runoff', 'rain', 'melt']    
##    df_gw.plot()
##    df_gw_cumsum = df_gw.cumsum() / 500000
##    df_gw_cumsum.plot()
##    df_gw_cumsum.to_pickle(r'C:\Users\Ch.Kienholz\Dropbox\Mendenhall\MB_model_calibration\water_level_modeled_cumsum.pickle')
#    
#    kwargs['sensitivity_test_switch'] = 2
#    kwargs['precip_adapt'] = 20
#    
#    mb_model(**kwargs)
#    
#    kwargs['sensitivity_test_switch'] = 1
#    kwargs['temp_grad_sel'] = -0.5
#    
#    mb_model(**kwargs)
#            
#    kwargs['temp_grad_sel'] = -1.0
#    
#    mb_model(**kwargs)
#    
#    kwargs['sensitivity_test_switch'] = 12
#    kwargs['precip_adapt'] = 20
#    kwargs['temp_grad_sel'] = -1.0
#             
#    mb_model(**kwargs)
#    
#    kwargs['sensitivity_test_switch'] = 123
#    kwargs['precip_adapt'] = 20
#    kwargs['temp_grad_sel'] = -1.0
#    kwargs['rfs_ratio'] = 0.8
#    kwargs['mfs_ratio'] = 0.8 
#             
#    mb_model(**kwargs)