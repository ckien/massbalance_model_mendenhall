# Massbalance model Suicide Basin

### About ###

Python-based model to estimate the glacier massbalance and runoff from the Suicide Basin watershed

### Run the model ###

```python
from massbalance_model import MB_Model

# create an instance of the MB_Model class    
model = MB_Model(climate_data_switch='basin', melt_model_switch='ETI')

# melt factor and radiation factor of ice for test run
# divide by 24 to get hourly values    
mf = 4.46 / 24.0
rfi = 0.006 / 24.0   

# run the model
model.run(mf, rfi)

# plot modeled and measured cumulative massbalances
model.plot_massbalances()
```

### Model calibration ###

Implemented in the spotpy package, see script *calibration_spotpy.py*.

### To dos ###

Recalibrate model with 2019 massbalance measurements