#!/usr/bin/env python

from collections import OrderedDict
    
class colors_ck():
    
    def __init__(self):

        colornamelist = ['orange', 'orange_light', 'green', 'green_light',
        'purple_light', 'purple', 'blue', 'blue_light', 'red_light', 'red',
        'brown', 'brown_light', 'darkgray']    
        
        colorlist = [[255,127,0], [253,191,111], [51,160,44], [178,223,138],
                     [202,178,214], [106,36,194], [25,116,210], [166,206,227],
                     [252,116,94],[227,26,28], [106,53,24],[177,89,40],[50,50,50]]  
    
        numbers = range(0, len(colornamelist), 1)
        colordict = {}
    
        for n in numbers:
            colorlist[n][0] = colorlist[n][0]/255.0
            colorlist[n][1] = colorlist[n][1]/255.0
            colorlist[n][2] = colorlist[n][2]/255.0
    
        for n, colorname in zip(numbers, colornamelist):
            colordict[colorname] = colorlist[n]
            
        self.param = colordict
        self.__dict__.update(colordict)
        
def clean_up_legend(ax):
    '''
    returns new [handles, labels]
    ''' 
    handles, labels = ax.get_legend_handles_labels()
    # remove duplicate lables and handles
    by_label = OrderedDict(zip(labels, handles))
    # sort both labels and handles by labels
    labels, handles = zip(*sorted(zip(by_label.keys(), by_label.values()), 
                                  key=lambda t: t[0]))
    
    return [handles, labels]
    
def annotatefun(ax, textlist, xstart, ystart, ydiff=0.05, fonts=12, 
                col='k', ha='left'):
    counter = 0
    for textstr in textlist:
        ax.text(xstart, (ystart - counter * ydiff), textstr, 
                fontsize=fonts, ha=ha, va='center', 
                transform=ax.transAxes, color=col)
        counter += 1